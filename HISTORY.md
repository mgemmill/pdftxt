### pdftxt


v0.3.2
------
- Fix to visual display of html output
- Created explicity UTF-8 conversion to html output,
  using the `replace` option.


v0.3.1
------

- Added `--pages` cli option.


v0.3.0
------

- Added `column_boundaries` option for `analyze_grid` method.


v0.2.0
------

- initial release


v0.1.0
------

- initial development
