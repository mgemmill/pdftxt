from pathlib import Path
from types import SimpleNamespace
import pytest

#  pytest_plugins = ['pytest-cov']


test_dir = Path(__file__).parent
sample_dir = test_dir / "samples"

_sample_files = SimpleNamespace()

_sample_files.word_pdf = sample_dir / "Word_PDF.pdf"
_sample_files.excel_pdf = sample_dir / "Grid_PDF_2.pdf"
_sample_files.excel_pdf_1 = sample_dir / "Grid_PDF_1.pdf"


@pytest.fixture()
def sample_files():
    return _sample_files
