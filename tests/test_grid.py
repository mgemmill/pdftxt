from pdftxt.api import PdfTxtContext, PdfTxtParams, Region
from pdftxt.grid import fetch_column_boundaries, fetch_row_boundaries, iter_cell_bounds
from pdftxt.grid import fetch_table_cells, table_cells_to_regions
from pdftxt.grid import allocate_characters_to_table
from pdftxt.grid import CollapsedCharacter


def print_text(text_list):
    print("-" * 60)
    for t in text_list:
        print(t)


def test_excel_pdf_grid_column_boundaries(sample_files):

    with PdfTxtContext(sample_files.excel_pdf) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            assert len(chars) == 300
            char_pts = fetch_column_boundaries(chars, 50, 500)
            assert char_pts == [55.5, 158, 265, 372, 484.5]


def test_excel_pdf_grid_row_boundaries(sample_files):

    with PdfTxtContext(sample_files.excel_pdf) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            assert len(chars) == 300
            char_pts = fetch_row_boundaries(chars, 490, 740)
            assert char_pts == [491, 516, 541, 566, 591, 616, 641, 666, 691, 716, 740]


def print_list(l):
    for c in l:
        print(c)


def test_collapsed_character_class(sample_files):
    with PdfTxtContext(sample_files.excel_pdf_1) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            # test single
            char = chars[50]
            assert char.x0 == 66.9992
            assert char.x1 == 74.1992
            assert char.y0 == 675.4
            assert char.y1 == 695.812

            wchar = CollapsedCharacter(char)
            assert wchar.x0 == 69.9992
            assert wchar.x1 == 70.9992
            assert wchar.y0 == 685.4  # 675.4 + 10
            assert wchar.y1 == 686.4  # 675.4 + 11

            assert repr(wchar) == '<CollapsedCharacter 69.9992,685.4,70.9992,686.4 "7">'

            CollapsedCharacter.collapse(chars)
            wchar2 = chars[50]
            assert wchar.y0 == wchar2.y0
            assert wchar.y1 == wchar2.y1

            CollapsedCharacter.uncollapse(chars)

            char2 = chars[50]
            assert char.y0 == char2.y0
            assert char.y1 == char2.y1


def test_excel_pdf_grid_row_boundaries_using_collapsed_character(sample_files):

    with PdfTxtContext(sample_files.excel_pdf_1) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            CollapsedCharacter.collapse(chars)
            assert len(chars) == 300
            char_pts = fetch_row_boundaries(chars, 490, 740)
            assert len(char_pts) == 11
            assert char_pts == [539, 598, 614, 630, 646, 662, 678, 694, 710, 726, 738]


def test_iter_cell_bounds():
    assert list(iter_cell_bounds((1, 2, 3, 4, 5))) == [(1, 2), (2, 3), (3, 4), (4, 5)]


def test_fetch_table_cells():
    row_bounds = (0, 1, 2, 3, 4, 5)
    col_bounds = (0, 1, 2, 3, 4, 5)

    table = fetch_table_cells(row_bounds, col_bounds)

    assert table_cells_to_regions(table) == [
        [(0, 0, 1, 1), (1, 0, 2, 1), (2, 0, 3, 1), (3, 0, 4, 1), (4, 0, 5, 1)],
        [(0, 1, 1, 2), (1, 1, 2, 2), (2, 1, 3, 2), (3, 1, 4, 2), (4, 1, 5, 2)],
        [(0, 2, 1, 3), (1, 2, 2, 3), (2, 2, 3, 3), (3, 2, 4, 3), (4, 2, 5, 3)],
        [(0, 3, 1, 4), (1, 3, 2, 4), (2, 3, 3, 4), (3, 3, 4, 4), (4, 3, 5, 4)],
        [(0, 4, 1, 5), (1, 4, 2, 5), (2, 4, 3, 5), (3, 4, 4, 5), (4, 4, 5, 5)],
    ]


def test_allocate_characters_to_table(sample_files):

    with PdfTxtContext(sample_files.excel_pdf) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            assert len(chars) == 300
            columns = fetch_column_boundaries(chars, 50, 500)
            rows = fetch_row_boundaries(chars, 490, 740)
            table = fetch_table_cells(rows, columns)
            allocate_characters_to_table(chars, table)
            assert table[0][0].chars_to_str() == "A01111111111"
            assert table[1][1].chars_to_str() == "B0222222222"
            assert table[2][2].chars_to_str() == "C033333333"
            assert table[3][3].chars_to_str() == "D04444444"
            assert table[9][0].chars_to_str() == "A00"
            assert table[8][1].chars_to_str() == "B099"
            assert table[7][2].chars_to_str() == "C0888"


def test_analyze_grid(sample_files):

    with PdfTxtContext(sample_files.excel_pdf) as pdf:
        for page in pdf:
            region = Region(x0=50, y0=490, x1=500, y1=740)
            params = PdfTxtParams()
            table = page.analyze_grid(region, params)

            # Test Cell.text property
            assert table[0][0].text == "A01111111111\n"
            assert table[1][1].text == "B0222222222\n"
            assert table[2][2].text == "C033333333\n"
            assert table[3][3].text == "D04444444\n"
            assert table[9][0].text == "A00\n"
            assert table[8][1].text == "B099\n"
            assert table[7][2].text == "C0888\n"

            # test Cell.get_text() method
            assert table[0][0].get_text() == "A01111111111\n"
            assert table[1][1].get_text() == "B0222222222\n"
            assert table[2][2].get_text() == "C033333333\n"
            assert table[3][3].get_text() == "D04444444\n"
            assert table[9][0].get_text() == "A00\n"
            assert table[8][1].get_text() == "B099\n"
            assert table[7][2].get_text() == "C0888\n"

            # test str(Cell)
            assert str(table[0][0]) == "A01111111111\n"
            assert str(table[1][1]) == "B0222222222\n"
            assert str(table[2][2]) == "C033333333\n"
            assert str(table[3][3]) == "D04444444\n"
            assert str(table[9][0]) == "A00\n"
            assert str(table[8][1]) == "B099\n"
            assert str(table[7][2]) == "C0888\n"

            # test repr(Cell)
            assert repr(table[0][0]) == '<Cell 55.5,491,158,516 "A01111111111\n">'
            assert repr(table[1][1]) == '<Cell 158,516,265,541 "B0222222222\n">'
            assert repr(table[2][2]) == '<Cell 265,541,372,566 "C033333333\n">'
            assert repr(table[3][3]) == '<Cell 372,566,484.5,591 "D04444444\n">'
            assert repr(table[9][0]) == '<Cell 55.5,716,158,740 "A00\n">'
            assert repr(table[8][1]) == '<Cell 158,691,265,716 "B099\n">'
            assert repr(table[7][2]) == '<Cell 265,666,372,691 "C0888\n">'

            # test Cell.width property
            assert table[0][0].width == 102.5
            assert table[1][1].width == 107
            assert table[2][2].width == 107
            assert table[3][3].width == 112.5
            assert table[9][0].width == 102.5
            assert table[8][1].width == 107
            assert table[7][2].width == 107

            # test Cell.height property
            assert table[0][0].height == 25
            assert table[1][1].height == 25
            assert table[2][2].height == 25
            assert table[3][3].height == 25
            assert table[9][0].height == 24
            assert table[8][1].height == 25
            assert table[7][2].height == 25


def test_analyze_grid_1(sample_files):

    with PdfTxtContext(sample_files.excel_pdf_1) as pdf:
        for page in pdf:
            region = Region(x0=50, y0=570, x1=500, y1=750)
            params = PdfTxtParams(collapse_lines=True)
            table = page.analyze_grid(region, params)
            print("-" * 60)
            print(table)
            assert table[0][0].text == "A01111111111\n"
            assert table[1][1].text == "B0222222222\n"
            assert table[2][2].text == "C033333333\n"
            assert table[3][3].text == "D04444444\n"
            assert table[9][0].text == "A00\n"
            assert table[8][1].text == "B099\n"
            assert table[7][2].text == "C0888\n"


def test_analyze_rows(sample_files):

    with PdfTxtContext(sample_files.excel_pdf) as pdf:
        for page in pdf:
            region = Region(x0=50, y0=490, x1=500, y1=740)
            params = PdfTxtParams()
            rows = page.analyze_rows(region, params)

            assert rows[0].text == (
                "A01111111111\n" "B01111111111\n" "C01111111111\n" "D01111111111\n"
            )


def test_analyze_rows_1(sample_files):

    with PdfTxtContext(sample_files.excel_pdf_1) as pdf:
        for page in pdf:
            region = Region(x0=50, y0=570, x1=500, y1=750)
            params = PdfTxtParams(collapse_lines=True)
            rows = page.analyze_rows(region, params)

            print(rows[0].text)
            assert (
                rows[0].text == "A01111111111 B01111111111 C01111111111 D01111111111\n"
            )
