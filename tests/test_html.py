from types import SimpleNamespace
import pytest
from pdftxt.html import HTMLDocument


@pytest.mark.skip(reason="HTML output is still changing too much.")
def test_html_document():
    page = SimpleNamespace()
    page.pageid = 1
    page.width = 500
    page.height = 800

    html = HTMLDocument("TEST")
    html.start_page(page, 0, 0, 0, 0)

    txt = SimpleNamespace()

    def get_text(self):
        return self.text

    txt.get_text = get_text.__get__(txt, SimpleNamespace)
    txt.text = "TESTING"
    txt.width = 150
    txt.height = 15
    txt.y0 = 500
    txt.x0 = 72
    txt.y1 = 87
    txt.x1 = 222

    html.add_text_block(txt)

    txt.text = "AGAIN!"
    txt.y0 = 480
    txt.y1 = 495

    html.add_text_block(txt)

    html.end_page()

    assert html._inner_html() == (
        '<div id="page-1" class="page" style="width:500pt;height:800;">'
        '<div id="txt-1" class="text-block" style="width:150pt;height:15pt;top:285pt;left:72pt;" title="(72,500,222,87)">TESTING</div>'
        '<div id="txt-2" class="text-block" style="width:150pt;height:15pt;top:305pt;left:72pt;" title="(72,480,222,495)">AGAIN!</div>'
        "</div>"
    )
