from pdftxt import region
from types import SimpleNamespace
from pdftxt.util import parse_page_notation


def test_calculate_page_regions():
    regions = region.calculate_predefined_regions(100, 200)
    assert regions["full-page"] == (0, 0, 100, 200)
    assert regions["top-half"] == (0, 100, 100, 200)
    assert regions["bottom-half"] == (0, 0, 100, 100)
    assert regions["left-half"] == (0, 0, 50, 200)
    assert regions["right-half"] == (50, 0, 100, 200)
    assert regions["top-left"] == (0, 100, 50, 200)
    assert regions["top-right"] == (50, 100, 100, 200)
    assert regions["bottom-left"] == (0, 0, 50, 100)
    assert regions["bottom-right"] == (50, 0, 100, 100)


def test_fetch_region_with_predefined_argument():
    page = SimpleNamespace()
    page.height = 100
    page.width = 100

    _region = region.fetch_region(page, "top-half")

    assert _region.x0 == 0.0
    assert _region.y0 == 50.0
    assert _region.x1 == 100.0
    assert _region.y1 == 100.0


def test_fetch_region_with_custom_argument():
    page = SimpleNamespace()
    page.height = 100
    page.width = 100
    page.x1 = 100
    page.y1 = 100

    _region = region.fetch_region(page, "(25,25,90,90)")

    assert _region.x0 == 25.0
    assert _region.y0 == 25.0
    assert _region.x1 == 90.0
    assert _region.y1 == 90.0


def test_parse_page_notation():
    fnc = parse_page_notation("1")
    assert fnc(1) is True
    assert fnc(2) is False

    for notation in ("", "all", "1+", "1-"):
        fnc = parse_page_notation(notation)
        assert fnc(1) is True
        assert fnc(5) is True
        assert fnc(23) is True

    fnc = parse_page_notation("3-5")
    assert fnc(2) is False
    assert fnc(4) is True
    assert fnc(6) is False

    fnc = parse_page_notation("3,6,8")
    assert fnc(1) is False
    assert fnc(3) is True
    assert fnc(8) is True
    assert fnc(10) is False
