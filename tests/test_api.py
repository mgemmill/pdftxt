import pytest
from pdftxt.api import PdfTxtContext, Region, PdfTxtParams
from pdftxt.region import in2pt


def print_text(text_list):
    print("-" * 60)
    for t in text_list:
        print(t)


@pytest.mark.skip(reason="Review this test.")
def test_word_pdf(sample_files):

    with PdfTxtContext(sample_files.word_pdf) as pdf:
        #  page = next(pdf)
        for page in pdf:

            assert page.height.v == 792
            assert page.width.v == 612

            assert page.media_box == (0, 0, 612, 792)

            assert page.text[0].text == "DOCUMENT HEADER"
            assert page.text[10].text == "AAAAA"
            assert page.text[11].text == "EEEEEE"
            assert page.text[-1].text == "TEXT BOX CONTENT A"

            page.resort_text()

            for index, txt in enumerate(page.text):
                print(f"{index:03d}, {txt!r}")

            assert page.text[0].text == "DOCUMENT HEADER"
            assert page.text[10].text == "AAAAA"
            assert page.text[11].text == "BBBBB"
            assert page.text[-1].text == "TEXT BOX CONTENT C"

            txt_obj = page.text[0]

            assert str(txt_obj) == "DOCUMENT HEADER"


def test_pdf_page_characters(sample_files):

    with PdfTxtContext(sample_files.word_pdf) as pdf:
        for page in pdf:
            assert page
            assert len(page.characters) == 793

            assert page.x0 == 0
            assert page.y0 == 0
            assert page.x1 == 612
            assert page.y1 == 792


def test_pdf_page_filter_characters(sample_files):

    with PdfTxtContext(sample_files.word_pdf) as pdf:
        for page in pdf:
            chars = page.filter_characters()
            assert len(chars) == 793

            # filter bottom half of page
            chars = page.filter_characters(y1=page.y1 / 2)
            print_text(chars)
            assert len(chars) == 91

            # filter top half of page
            chars = page.filter_characters(y0=page.y1 / 2)
            print_text(chars)
            assert len(chars) == 701


def test_pdf_page_analyze(sample_files):

    with PdfTxtContext(sample_files.word_pdf) as pdf:
        for page in pdf:

            # analyze bottom half of page:
            region = Region(x0=0, y0=0, x1=page.x1, y1=(page.y1 / 2))
            params = PdfTxtParams(line_margin=0.01, exclude_white_space=False)
            text = page.analyze(region, params)

            print_text(text)
            assert len(text) == 12

            # analyze bottom half of page, but exclude any white space text boxes:
            params.exclude_white_space = True
            text = page.analyze(region, params)

            print_text(text)
            assert len(text) == 4

            # analyze a small section of the page:
            region = Region(x0=in2pt(1), y0=in2pt(1.7), x1=in2pt(2.75), y1=in2pt(2.5))
            params = PdfTxtParams()

            text = page.analyze(region, params)

            print_text(text)
            assert len(text) == 1
            assert text[0].get_text().strip() == "TEXT BOX CONTENT C"
